new-books
=========

'nb' monitors a watchlist of authors and sends an email alert whenever a new book is published.
It is useful for keeping track of favorite authors to be notified when they release a new book.
It uses LibraryThing as a database but doesn't require an account there.

Example email output:

    Date: Wed, 11 Apr 2012 06:10:44 -0400
    From: notify@ab.net
    To: stephen@ab.net
    Subject: New Books at LibraryThing

    1 new work for Mark Lynas (lynasmark)

    * The God Species: How Humans Really Can Save the Planet...

    http://www.librarything.com/author/lynasmark 


See nb.txt for docs
