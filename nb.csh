#!/bin/tcsh
# ---------------------------------------------------------------
# New Books by Stephen Balbach
#
# 'nb' monitors a watchlist of authors and sends an email alert whenever a new book is published.
# It is useful for keeping track of favorite authors to be notified when they release a new book.
#
# Home site at http://bachlab.balbach.net/nb or https://github.com/stbalbach/new-books
#
# Since LibraryThing changes HTML formating occasionally, if it stops working (eg. continual "Bad HTML" errors),
# a new version will be made available. In practice it's been stable for years.
#
# "nb" -- ver.1.85 (May 2014)
#         ver.1.8 (May 2014)
#         ver.1.7 (March 2011)
# ---------------------------------------------------------------

# CUSTOMIZE START

set NB_PATH="/home/notify/nb/"   # The full path where nb is located (w/ trailing slash)
set NB_MAIL="/usr/bin/mailx"     # Email program. Might need to modify last line of this code if not using mailx
set NB_EMAIL="stephen@localhost" # Email address for alerts

                           # Versions the script was developed and tested with.
                           # tcsh 6.14.00
set WGET="/usr/bin/wget"   # GNU Wget 1.10.2
set AWK="/usr/bin/awk"     # GNU Awk 3.1.5
set UNIQ="/usr/bin/uniq"   # GNU uniq 5.93
set DIFF="/usr/bin/diff"   # GNU diff 2.8.7
set SORT="/bin/sort -dis"  # GNU sort 5.93

# Email warnings/errors (1=on, 0=off)

set email_errors   = 1    # Recommend always on
set email_warnings = 1    # Recommend on at first, turn off later. If getting
                          # too many warnings, change time of day script runs

# END CUSTOMIZE

setenv LANG en_US.UTF-8
setenv LC_COLLATE en_US.UTF-8

if ( $#argv != 1 ) then
  echo "\nUsage: /home/user/nb/nb /home/user/nb/nb.cfg\n"
  exit(1)
endif
if ( ! -e $argv[1] ) then
  echo "\nUsage: /home/user/nb/nb /home/user/nb/nb.cfg\n"
  exit(1)
endif

set emailbody = "$NB_PATH""emailbody.txt"
echo "LibraryThing New Books\n" > "$emailbody"

set newworksflag = 0

foreach authorname (`$AWK 'NF' "$argv[1]"`)

  set baseurl = "http://www.librarything.com/author/""$authorname"
  set fullurl = "http://www.librarything.com/author/""$authorname"'&all=books'
  set newfilename = "$NB_PATH""$authorname"".new"
  set oldfilename = "$NB_PATH""$authorname"".old"
  set workfilename = "$NB_PATH""$authorname"".htm"

# Get the HTML data and check for any error conditions

  $WGET --header="Cookie: LTUnifiedCookie=%7B%22areyouhuman%22%3A1%7D" -q -O- "$fullurl" > "$workfilename"
  set authornamefull = `$AWK -F">" '/<title>/ {split($2,a,"|")} END {print a[1]}' "$workfilename"`
  set existingworks = `$AWK '/donthave/ {s=$0} END {while (sub("title","t",s)) i++; print i}' "$workfilename"`
  set highload = `$AWK -v i=0 'tolower($0) ~ /highload/ {i++} END {print i}' "$workfilename"`
  set tempdown = `$AWK -v i=0 'tolower($0) ~ /temporarily down/ {i++} END {print i}' "$workfilename"`
  set downtime = `$AWK -v i=0 'tolower($0) ~ /downtime/ {i++} END {print i}' "$workfilename"`
  set failconn = `$AWK -v i=0 'tolower($0) ~ /failure to connect/ {i++} END {print i}' "$workfilename"`

  if ($existingworks > 0 && $highload == 0 && $tempdown == 0 && $downtime == 0 && $failconn == 0) then # no error conditions

    if( -e "$newfilename") then  # author tracked
      mv "$newfilename" "$oldfilename"
    else                         # author new
      echo "" > "$oldfilename"
    endif

# sorted list of all works by author ..

    $AWK '{RS=("<li |</li>")}/class=\"donthave\" /{split($0,a,"\""); print a[8]}' "$workfilename" | $SORT | $UNIQ >> "$newfilename"

# how many new works added since last check

    set numberofchanges = `$DIFF -bB "$oldfilename" "$newfilename" | $AWK '/^>/ {i++} END {print i}' `

    if ($numberofchanges > 0) then  # format the email
      set newworksflag = 1
      if ($numberofchanges == 1) echo "\n""$numberofchanges"' new work for '"$authornamefull"' ('"$authorname"')'"\n" >> "$emailbody"
      if ($numberofchanges > 1) echo "\n""$numberofchanges"' new works for '"$authornamefull"' ('"$authorname"')'"\n" >> "$emailbody"
      $DIFF -bB "$oldfilename" "$newfilename" | $AWK -F"> " '/^>/ {printf( " * %s\n",$2 ) }' >> "$emailbody"
    endif

  else #
    if ($existingworks == 0 && $highload == 0 && $tempdown == 0 && $downtime == 0 && $failconn == 0 && $email_errors == 1) then # HTML is bad
      set newworksflag = 1
      echo "\nERROR: Bad/unknown HTML (""$workfilename"") retrieved from ""$fullurl" >> "$emailbody"
    else if ($highload > 0 && $email_warnings == 1) then  # highload
      set newworksflag = 1
      echo "\nWARNING: LT reports High Load for ""$authorname"" - skipping for now ""$fullurl"  >> "$emailbody"
    else if ($tempdown > 0 && $email_warnings == 1) then # tempdown
      set newworksflag = 1
      echo "\nWARNING: LT reports Temp Down for ""$authorname"" - skipping for now ""$fullurl" >> "$emailbody"
    else if ($downtime > 0 && $email_warnings == 1) then # downtime
      set newworksflag = 1
      echo "\nWARNING: LT reports Down Time for ""$authorname"" - skipping for now ""$fullurl" >> "$emailbody"
    else if ($failconn > 0 && $email_warnings == 1) then # failconn
      set newworksflag = 1
      echo "\nWARNING: LT reports Failure to connect for ""$authorname"" - skipping for now ""$fullurl" >> "$emailbody"
    endif
  endif
end

# Email the results

if ($newworksflag == 1) then
  "$NB_MAIL" -s "New Books at LibraryThing" -a "$emailbody" "$NB_EMAIL" < /dev/null

endif

